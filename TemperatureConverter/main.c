/*
 * main.c
 * 
 * Copyright 2015 Unknown <user@manjaro>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>

int main(int argc, char **argv)
{
    float temp=0;
    float kelvin=0;
    float celsius=0;
    float fahrenheit=0;
    char unit;
	printf("Enter a temperature number\n");
    scanf("%f", &temp);
    printf("you entered %f, what unit are you using(F,C,K)", temp);
    scanf("%c,", &unit);
    if(unit=='c'){
		kelvin=temp-273.15;
		fahrenheit=temp*9/5+32;
		printf("%f Celsius =%f Fahrenheit and %f Kelvin", temp, fahrenheit, kelvin);
	}
	else if(unit=='f'){
		celsius=(fahrenheit-32)*5/9;
		kelvin=celsius-273.15;
		printf("%f Fahrenheit =%f Celsius and %f Kelvin", temp, fahrenheit, kelvin);
	}
	else if(unit=='k'){
		celsius=temp+273.15;
		fahrenheit=celsius*9/5+32;
		printf("%f Kelvin =%f fahrenheit and %f celsius", temp,fahrenheit, celsius);
	}
	else{
		printf("\n why did you do this");
	}
	return 0;
}
